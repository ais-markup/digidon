//= include/svg4everybody.js
//= include/jquery.formstyler.js

//-----footer down-----
function footerDown() {
    var $head = $('.header');
    var headH = 0;
    if ($head.length) {
        headH = $head.outerHeight();
    }
    var $foot = $('.footer');
    var footH = 0;
    if ($foot.length) {
        footH = $foot.innerHeight();
    }

    var $main = $('.main');
    $main.css({
        'min-height': ''
    });

    var heightContent = window.innerHeight - (headH + footH);
    $main.css({
        'min-height': heightContent
    });
}

//-----svg icons-----
function svgCrossbrowser() {
    svg4everybody();
}

//-----formstyler-----
function initFormstyler() {
    var elementForm = $('.formstyler');
    if (elementForm.length && $.fn.styler()) {
        elementForm.addClass('visible').styler();

        // $('a[data-toggle="tab"]').on('shown.bs.tab', function () {
        //    setTimeout(function () {
        //       $('select').trigger('refresh');
        //    }, 1)
        // })
    }
}

//-----height full block-----
function fullHeight() {
    var $block = $('.js-height-win');
    if ($block.length) {
        $block.each(function () {
            $(this).css({'height': window.innerHeight});
        });
    }
}

//-----search-drop-----
function _dropSearchClose($blindScroll, $blindStatic, $body, activeCLASS, $searchResult) {
    if ($blindScroll.length && $blindStatic.length) {
        $blindScroll.css({'padding-right': ''});
        $blindStatic.css({'padding-right': ''});
    }
    $body
      .removeClass(activeCLASS)
      .css({'padding-right': ''});
    if ($searchResult.length) {
        $searchResult.addClass('toggle-search-scroll-close');
    }
}

function dropSearch() {
    var $search = $('.js-drop-search');
    if ($search.length) {
        var $body = $('body');
        var $blindScroll = $('.blind-scroll');
        var $blindStatic = $('.blind-static');
        var $searchResult = $('#searchResults');
        var activeCLASS = 'search-drop_open';
        var s = window.innerWidth - document.body.clientWidth;

        //open
        $(document).on('click.searchBtn', '.js-drop-search-btn', function () {
            if ($blindScroll.length && $blindStatic.length) {
                $blindScroll.css({'padding-right': s + 'px'});
                $blindStatic.css({'padding-right': s + 'px'});
            }
            $body
              .css({'padding-right': s + 'px'})
              .addClass(activeCLASS);
        });

        //close
        $search.on('click.searchBtn', '.js-drop-search-close', function () {
            _dropSearchClose($blindScroll, $blindStatic, $body, activeCLASS, $searchResult);
        });

        //close
        $(document).off('click.search').on('click.search', function (event) {
            if ($body.hasClass(activeCLASS)) {
                if (
                  (
                    (!$(event.target).parents('.js-drop-search').length) &&
                    (!$(event.target).parents('.js-drop-search-btn').length) &&
                    (!$(event.target).parents('.search-results').length) &&
                    ((!$(event.target).hasClass('.js-drop-search-btn')))
                  )
                ) {
                    _dropSearchClose($blindScroll, $blindStatic, $body, activeCLASS, $searchResult);
                }
            }
        });
    }
}

//-----drop block-----
function dropBlock() {
    var $drop = $('.js-drop');
    if ($drop.length) {
        $drop.each(function () {
            var self = $(this);
            var $lnk = self.find('.js-drop-link');
            var $block = self.find('.js-drop-block');
            $lnk.on('click', function () {
                if (self.hasClass('active')) {
                    $block.fadeOut(200);
                    self.removeClass('active');
                } else {
                    $block.fadeIn(200);
                    self.addClass('active');
                }
            });
        });
    }
}

$(document).off('click.more').on('click.more', function (event) {
    var dropBlock = $('.js-drop-block');
    if (dropBlock.length && dropBlock.parents('.js-drop').hasClass('active')) {
        if (
          (!$(event.target).parents('.js-drop').length)
        ) {
            dropBlock.fadeOut(200);
            dropBlock.parents('.js-drop').removeClass('active');
        }
    }
});

//-----Tabs-----
function tabs() {
    var $block = $('.tabs');
    if (!$block.length) {
        return false;
    }
    $block.each(function () {
        var selfTabs = $(this);
        var $item = selfTabs.find('.tab');
        var $panel = selfTabs.find('.tab__panel');
        var wWidth = window.innerWidth;
        var heightList = '';

        $item.each(function () {
            var self = $(this);
            var selfP = self.find('.tab__panel');
            var heightItem = '';

            if (wWidth > 1024) {
                if (self.hasClass('active')) {
                    heightList = selfP.innerHeight();
                }
            } else {
                if (self.hasClass('active')) {
                    heightItem = selfP.find('.tab__panel-wrap').innerHeight();
                } else {
                    heightItem = '';
                }
            }

            selfP.css({'height': heightItem});

            self.off('click').on('click', function () {
                $item.each(function () {
                    $(this).removeClass('active');
                });
                $panel.each(function () {
                    $(this)
                      .removeClass('active')
                      .css({'height': ''});
                });

                if (wWidth > 1024) {
                    var thPadding = self.find('.tab__panel-wrap').innerHeight();
                    selfTabs.css({'padding-bottom': thPadding});
                }
                else {
                    selfTabs.css({'padding-bottom': ''});
                    selfP.css({'height': selfP.find('.tab__panel-wrap').innerHeight()});
                }
                self.addClass('active');
                selfP.addClass('active');
            });
        });

        selfTabs
          .addClass('tabsActive')
          .css({'padding-bottom': heightList});
    });
}

//-----END--Services-----

$(function () {
    footerDown();
    fullHeight();
    dropSearch();
    initFormstyler();
    dropBlock();
});
$(window).on('resize', function () {
    footerDown();
    fullHeight();
    tabs();
});
$(window).on('load', function () {
    footerDown();
    svgCrossbrowser();
    initFormstyler();
    tabs();
});