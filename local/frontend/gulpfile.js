'use strict';

const gulp = require('gulp');
const del = require('del');
const rename = require('gulp-rename');
const newer = require('gulp-newer'); // измененные файлы

const fileinclude = require('gulp-file-include');

const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const sourcemaps = require('gulp-sourcemaps');
const mmq = require('gulp-merge-media-queries');

const rigger = require('gulp-rigger');
const uglify = require('gulp-uglify');

const imagemin = require('gulp-imagemin');
const svgSprite = require('gulp-svg-sprite');
const cheerio = require('gulp-cheerio');
const gulpIf = require('gulp-if');

const browserSync = require('browser-sync').create();

// const spritesmith = require('gulp.spritesmith'); //png-sprite
// var imagesNormalizer = require('gulp-retina-sprites-normalizer'); //png-sprite

// var mmq = require('gulp-merge-media-queries');

// const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';

//-----html------
gulp.task('html', function () {
  return gulp.src(['src/pages/*.html'])
      .pipe(fileinclude({
        prefix: '@@',
        basepath: '@file',
        indent: true
      }))
      .pipe(gulp.dest('build'));
});


//-----styles------
gulp.task('styles', function () {
  // const postcssOptions = [require('autoprefixer'), require('cssnano')];
  const postcssOptions = [autoprefixer, cssnano];

  return gulp.src('src/scss/style.scss')
      .pipe(sass())
      .pipe(mmq({log: true}))
      .pipe(postcss(postcssOptions))
      .pipe(rename({suffix: '.min'}))
      .pipe(gulp.dest('build/css'));
});

gulp.task('styles-dev', function () {
  return gulp.src('src/scss/style.scss')
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(mmq({log: true}))
    .pipe(postcss([ autoprefixer() ]))
    .pipe(rename({suffix: '.min'}))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('build/css'));
});


//-----script------
gulp.task('script', function () {
  return gulp.src('src/js/main.js')
    .pipe(rigger())
    .pipe(uglify())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('build/js'));
});
gulp.task('scriptlibs', function () {
  return gulp.src('src/js/_libs/**/*.js')
    .pipe(uglify())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('build/js/libs'));
});


//-----images------
gulp.task('images', function () {
  return gulp.src(['src/images/**/*.{png,jpg,gif}'], {since: gulp.lastRun('images')})
      .pipe(newer('build/images'))
      .pipe(imagemin())
      .pipe(gulp.dest('build/images'));
});

gulp.task('images-dev', function () {
  return gulp.src(['src/images/**/*.{png,jpg,gif}'], {since: gulp.lastRun('images')})
      .pipe(newer('build/images'))
      .pipe(gulp.dest('build/images'));
});

gulp.task('images-svg', function () {
  return gulp.src(['src/images/**/*.svg'], {since: gulp.lastRun('images')})
      .pipe(newer('build/images'))
      .pipe(gulp.dest('build/images'));
});

//-----fonts-----
gulp.task('fonts', function () {
  return gulp.src(['src/fonts/*.*'], {since: gulp.lastRun('fonts')})
      .pipe(newer('build/fonts'))
      .pipe(gulp.dest('build/fonts'));
});
//-----js-----
gulp.task('js', function () {
  return gulp.src(['src/scripts/*.*'])
      .pipe(newer('build/scripts'))
      .pipe(gulp.dest('build/scripts'));
});


//-----sprites------
gulp.task('spritesvg', function () {
  return gulp.src('src/sprites/svg/**/*.svg')
      .pipe(cheerio({
        run: function ($) {
          $('[fill]').removeAttr('fill');
          $('[stroke]').removeAttr('stroke');
          $('[style]').removeAttr('style');
        },
        parserOptions: {xmlMode: true}
      }))
      .pipe(svgSprite({
        mode: {
          symbol: {
            dest: '.',
            sprite: 'sprite.svg',
            render: {
              scss: {
                dest: '_sprite_svg.scss',
                template: 'src/scss/_spritesvgtemp.scss'
              }
            }
          }
        },
        shape: {
          dimension: {
            maxWidth: 32,
            maxHeight: 32
          }
        }
      }))
      .pipe(gulpIf('*.scss', gulp.dest('src/scss'), gulp.dest('build/images')));
});

gulp.task('spritesvgbg', function () {
  return gulp.src('src/sprites/svg-bg/**/*.svg')
    .pipe(svgSprite({
      shape: {
        spacing: {
          padding: 5
        }
      },
      mode: {
        css: {
          dest: ".",
          layout: "diagonal",
          sprite: 'sprite-bg.svg',
          bust: false,
          render: {
            scss: {
              dest: "_sprite_svg-bg.scss",
              template: "src/scss/_spritesvgtemp_bg.scss"
            }
          }
        }
      },
      variables: {
        mapname: "icons"
      }
    }))
    .pipe(gulpIf('*.scss', gulp.dest('src/scss'), gulp.dest('build/images')));
});

gulp.task('serve', function () {
  browserSync.init({
    server: 'build'
  });

  browserSync.watch('build/**/*.*').on('change', browserSync.reload)
});


//-----clear build------
gulp.task('clear', function () {
  return del(['build']);
});

gulp.task('build', gulp.series(
    'clear',
    'spritesvg',
    'spritesvgbg',
    gulp.parallel('html', 'styles', 'script', 'scriptlibs', 'fonts', 'images', 'images-svg')
));

gulp.task('watch', function () {
  gulp.watch('src/pages/*.html', gulp.series('html'));
  gulp.watch('src/temp/**/*.html', gulp.series('html'));
  gulp.watch('src/scss/**/*.*', gulp.series('styles-dev'));
  gulp.watch('src/js/**/*.*', gulp.series('script'));
  gulp.watch('src/scripts/*.*', gulp.series('js'));
  gulp.watch('src/fonts/*.*', gulp.series('fonts'));
  gulp.watch('src/images/**/*.{png,jpg,gif}', gulp.series('images-dev'));
  gulp.watch('src/images/**/*.svg', gulp.series('images-svg'));
});

gulp.task('dev', gulp.series(
  'clear',
  'spritesvg',
  'spritesvgbg',
  gulp.parallel('html', 'styles-dev', 'script', 'scriptlibs', 'fonts', 'js', 'images-dev', 'images-svg', 'watch', 'serve')

));
